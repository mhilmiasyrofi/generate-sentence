from __future__ import print_function
import random
from utils import generateSixDigit
import operator


if __name__ == '__main__':

    listFile = open("list.txt", "w")
    
    arr = []
    map_digit = {}
    for i in range(0, 100000):
        n = generateSixDigit()
        if n in arr :
            map_digit[n] += 1    
        else :
            map_digit[n] = 1
            arr.append(n)
    
    # sort map
    sorted_map = sorted(map_digit.items(), key=operator.itemgetter(1))
    # inverse map
    inv_map = {v: k for k, v in map_digit.items()}

    for v in sorted_map :
        # print(v)
        listFile.write(str(v[0]))
        listFile.write(" ")
        listFile.write(str(v[1]))
        listFile.write("\n")

    listFile.close()
    

