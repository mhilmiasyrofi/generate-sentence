from __future__ import print_function
import random
import readkbbi
from readkbbi import readKBBI
import re


class WordTable:
    word = ""
    prev = None
    next = None

    def __init__(self, word="blank"):
        self.word = word
        self.prev = []
        self.next = []

    def printData(self):
        print(self.word)
        for i in range(0, len(self.prev)):
            print("\t" + self.prev[i])
            print("\t" + self.next[i])

class Keyword :
    word = ""
    rule = ""

    def __init__(self, w, r) :
        self.word = w
        self.rule = r

class Sentence:
    sentence = ""
    otp = ""

    def __init__(self, s="", o=""):
        self.sentence = s
        self.otp = o 

mapping_otp = {
    "u": 0,
    "t": 1,
    "k": 2,
    "o": 3,
    "i": 4,
    "a": 5,
    "r": 6,
    "n": 7,
    "e": 8,
    "s": 9
}

mapping_digit = {
    0 : "u", 
    1 : "t", 
    2 : "k", 
    3 : "o", 
    4 : "i", 
    5 : "a", 
    6 : "r", 
    7 : "n", 
    8 : "e", 
    9 : "s"
 }

def isWordUnique(word):
    count = 0
    for i in range(0, len(word)):
        for j in range(0, 10):
            if word[i:][:2] == mapping_digit[j]:
                count += 1
    if count < 2:
        return True
    else:
        return False


def readData(fileName):
    data = []
    with open(fileName, "r") as file:
        for line in file:
            words = line.split()
            # if len(words) >=  5 :
            #     for w in words :
            #         print(w, end=' ')
            #     print()
            data.append(words)
    return data

def generateSixDigit():
    n = random.randint(100000, 999999)
    # n = 590194
    return n

def generateMappingTable() :
    
    mtable = []
    n_otp = ""
    while (len(mtable) < 10) :
        n = random.randint(0, 9)
        if n not in mtable:
            mtable.append(n)
            if len(n_otp) < 6 :
                n_otp = n_otp + str(n)

    return mtable


def generateOtp(mtable):

    mapping_digit = {
        mtable[0] : "a", 
        mtable[1] : "i", 
        mtable[2] : "e", 
        mtable[3] : "n", 
        mtable[4] : "r", 
        mtable[5] : "s", 
        mtable[6] : "t", 
        mtable[7] : "k", 
        mtable[8] : "o", 
        mtable[9] : "u"
    }


    otp = []
    n = generateSixDigit()
    m = n
    # print("otp: ", end='')
    # print(n)
    i = 5
    while (i >= 0):
        otp.append(mapping_digit[n % 10])
        n /= 10
        i -= 1
    otp.reverse()

    return otp, m


def decodeWordToOtp(word):
    ret = ""
    for i in range(0, len(word)):
        for j in range(0, 10):
            if word[i:][:2] == mapping_digit[j]:
                ret += str(j)
    return ret


def decodeWordToOtpReverse(word):
    ret = ""
    for i in range(0, len(word)):
        for j in range(0, 10):
            if word[i:][:2] == mapping_digit[j]:
                ret = str(j) + ret
    return ret

def isOtp(word):
    for i in range(0, 10):
        if mapping_digit[i] == word:
            return True
    return False


def decodeSentenceToBinary(s):
    l = len(s)
    ret = ""
    for i in range(0, l-1):
        if isOtp(s[i:][:2]):
            ret += "1"
        else:
            ret += "0"
    return ret


exception = ["para", "dan", "dari", "ke", "di", "dengan", "yaitu", "agar", "dapat", "terhadap", "yang" , "berbagai"]

def generateSentence(data, otp, mtable):


    mapping_otp = {
        "a": mtable[0],
        "i": mtable[1],
        "e": mtable[2],
        "n": mtable[3],
        "r": mtable[4],
        "s": mtable[5],
        "t": mtable[6],
        "k": mtable[7],
        "o": mtable[8],
        "u": mtable[9]
    }

    # print(mapping_otp)
    # print(otp)

    sentence = []

    # for d in data :
    #     print(d)

    random.shuffle(data)

    for kalimat in data:
        # print(kalimat)
        i = 0
        sentence = []
        for k in kalimat :
            k = k.lower()
            arr = {}
            if k.find(otp[i]) != -1 :
                arr = {'word' : k, 'idx' : k.find(otp[i])}
                i += 1
                sentence.append(arr)
            else :
                if i != 0 :
                    arr = {'word' : k, 'idx' : -1} 
                    sentence.append(arr)
            if i == 6:
                # print(i)
                # print(sentence)
                # print(kalimat)
                l = 0
                gagal = False
                
                for x in exception :
                    if sentence[-1]['word'] == x :
                        # print(sentence)
                        # print(x)
                        j = 0
                        for j in range(0, len(kalimat)-1) :
                            if kalimat[j].lower() == sentence[-2]['word'] and kalimat[j+1].lower() == sentence[-1]['word'] :
                                # print(kalimat[j])
                                # print(kalimat[j+1])
                                arr = {'word' : kalimat[j+2].lower(), 'idx' : -1}
                                sentence.append(arr)
                                break
                        break

                # print("SELESAI")
                # print(sentence)

                for s in sentence :
                    l += len(s['word'])
                    l += 1
                    if s['word'].find('.') != -1 or s['word'].find(',') != -1 or s['word'].find('(') != -1 or s['word'].find(')') != -1 or s['word'].find('-') != -1:
                        gagal = True
                        break
                if gagal :
                    i = 0
                    continue

                # print("PANJANG")
                # print(l)
                if l < 80 :
                    return sentence
                else :
                    i = 0
                    continue
    return []


def convertBinaryToAscii(biner):
    asci = 0
    i = len(biner) - 1
    while (i >= 0):
        if (biner[i] == "1"):
            asci += 2 ** (len(biner)-i - 1)
        i -= 1
    return asci

def convertAsciToBinary(asci):
    return '{0:06b}'.format(asci)

def decodeBinarytoAscii(biner):
    i = 0
    sub_biner = []
    added_biner = []
    asci_arr = []
    while (i < len(biner)):
        sub_biner.append(biner[i:][:5])
        i += 5
    if len(sub_biner[-1]) < 5:
        if len(sub_biner[-1]) == 4:
            sub_biner[-1] += "0"
        elif len(sub_biner[-1]) == 3:
            sub_biner[-1] += "00"
        elif len(sub_biner[-1]) == 2:
            sub_biner[-1] += "000"
        elif len(sub_biner[-1]) == 1:
            sub_biner[-1] += "0000"

    for b in sub_biner:
        added_biner.append("10" + b)

    # print(added_biner)

    for b in added_biner:
        asci_arr.append(convertBinaryToAscii(b))

    return asci_arr


def encodeAsciArr(arr):
    ret = ""
    for a in arr:
        if a >= 65 and a <= 122:
            # if chr(a) == 'q' or chr(a) == 'Q':
            #     ret += 'r'
            # elif chr(a) == 'v' or chr(a) == 'V':
            #     ret += 'u'
            # elif chr(a) == 'x' or chr(a) == 'X':
            #     ret += 'w'
            # elif chr(a) == 'z' or chr(a) == 'Z':
            #     ret += 'y'
            # else:
            #     ret += chr(a)
            ret += chr(a)
        elif a < 65:
            ret += '*'
            ret += chr(65 + 65 - a)
        else:
            ret += '*'
            ret += chr(65 + a - 90)

    return ret

def decodeAsciArr(arr) :
    ret = []
    for a in arr:
        if a[0] == '*' :
            ret.append(65-(ord(a[1].upper())-65))
        else :
            ret.append(ord(a[0].upper()))
    
    return ret

def convertAsciArrToBiner(arr) :
    ret = []
    for a in arr :
        ret.append(convertAsciToBinary(a))
    return ret


def isVowel(c):
    return c == 'A' or c == 'I' or c == 'U' or c == 'E' or c == 'O' or c == 'a' or c == 'i' or c == 'u' or c == 'e' or c == 'o'


def matchAsciWithEjaan(asci, eja, mark):
    ret = None
    if mark:
        if isVowel(asci):
            if len(eja) == 1 and eja[0] == asci:
                ret = True
            else:
                ret = False
        else:
            if len(eja) == 4 and eja[0] == asci:
                ret = True
            else:
                ret = False
    else:
        if isVowel(asci):
            if len(eja) == 2 and eja[1] == asci:
                ret = True
            else:
                ret = False
        else:
            if len(eja) == 3 and eja[0] == asci:
                ret = True
            else:
                ret = False
    return ret

def geserMarker(arr) :
    temp = []
    for i in range(0, len(arr)-1) :
        temp.append(arr[i])
    temp.insert(0, arr[-1])

    return temp

def geserKeyword(keyword, sesi_mod) :
    
    k = []

    k.append(Keyword(keyword[0].word, keyword[-1].rule))
    for i in range(1, len(keyword)) :
        word = keyword[i].word
        rule = keyword[i-1].rule
        k.append(Keyword(word, rule))

    return k 

def geserBalikMarker(arr) :
    temp = []
    for i in range(1, len(arr)) :
        temp.append(arr[i])
    temp.append(arr[0])

    return temp

def generateKeyword(asci, words, sesi_mod):

    keywords = []
    keyword = []

    ganjil = False
    asci_arr = []
    marker = []

    i = 0
    while i < len(asci) :
        if asci[i] ==  '*':
            marker.append(True)
            i += 1
        else :
            marker.append(False)
        asci_arr.append(asci[i])
        i += 1

    # for i in range(0, sesi_mod) :
    #     marker = geserMarker(marker)   

    # for i in range(len(marker)) :
    #     print(asci_arr[i])
    #     print(marker[i])

    # removed_star_asci = re.sub('[*]', '', asci)

    # print(removed_star_asci)
    # print(asci)

    n = len(asci_arr)
    # print(n)
    if n % 2 == 1 :
        ganjil = True

    if ganjil :
        for w in words :
            keyword = []
            if len(asci_arr[-1])  == 1 :
                if w.word[0] == asci_arr[-1] :
                    word = Keyword(w.word, ' ')
                    keyword.append(word)
                    keywords.append(keyword)
            else :
                if w.word[0] == asci_arr[-1] :
                    word = Keyword(w.word, ' :')
                    keyword.append(word)
                    keywords.append(keyword)
            
    else :
        # print("GENAP")
        for w in words:
            keyword = []
            if w.word[0] == asci_arr[-2] and w.word[-1] == asci_arr[-1]:
                word = Keyword(w.word, ',')
                if marker[-2] and marker[-1] :
                    word.rule = '.'
                elif marker[-2] :
                    word.rule = ':'
                elif marker[-1] :
                    word.rule = ';'

                keyword.append(word)
                # print(word.word)
                keywords.append(keyword)


    while len(keywords) > 0 :
        k = keywords[0]
        # print(n)
        # print(len(k))


        if len(k)*2 < n :
            first = None
            last = None
            first_idx = None
            last_idx = None
            if ganjil :
                if len(k) % 2 == 1 :
                    first = asci_arr[n-len(k)*2]
                    last = asci_arr[n-len(k)*2-1]
                    first_idx = n-len(k)*2
                    last_idx = n-len(k)*2-1
                else :
                    first = asci_arr[n-len(k)*2-1]
                    last = asci_arr[n-len(k)*2]
                    first_idx = n-len(k)*2-1
                    last_idx = n-len(k)*2
            else :
                if len(k) % 2 == 1 :
                    first = asci_arr[n-len(k)*2-1]
                    last = asci_arr[n-len(k)*2-2]
                    first_idx = n-len(k)*2-1
                    last_idx = n-len(k)*2-2
                else :
                    first = asci_arr[n-len(k)*2-2]
                    last = asci_arr[n-len(k)*2-1]
                    first_idx = n-len(k)*2-2
                    last_idx = n-len(k)*2-1
            
            for w in words:
                if len(w.word) >= 3 :
                    if w.word[0] == first and w.word[-1] == last:
                        word = Keyword(w.word, ',')
                        if marker[last_idx] and marker[first_idx]:
                            word.rule = '.'
                        elif marker[first_idx]:
                            word.rule = ':'
                        elif marker[last_idx]:
                            word.rule = ';'
                        is_unique = True
                        for x in k :
                            if w.word == x.word:
                                is_unique = False
                                break
                                # print(x.word)
                        if is_unique :
                            k_temp = list(k)
                            k_temp.append(word)
                            keywords.append(k_temp)
                            break
        else :
            return k
        keywords.pop(0)
    return []

def encodeBiner(idx, word) :
    biner = bin(idx+1)[2:]
    l = len(biner)
    # while l < len(word) :
    while l < 4 :
        l += 1
        biner = '0' + biner

    # print("ENCODE BIN")
    # print(idx)
    # print(biner)
    return biner

def decode(gabungan, sesi_mod, mtable) :

    mapping_digit = {
        mtable[0] : "a", 
        mtable[1] : "i", 
        mtable[2] : "e", 
        mtable[3] : "n", 
        mtable[4] : "r", 
        mtable[5] : "s", 
        mtable[6] : "t", 
        mtable[7] : "k", 
        mtable[8] : "o", 
        mtable[9] : "u"
    }


    s = gabungan.split('#')
    kalimat = s[0]
    keyword = s[1]

    i = 0
    word = []
    rule = []
    s = ""
    while i < len(keyword) :
        if keyword[i] == ' ' :
            rule.append(5)
            word.append(s)
            s = ""
        elif keyword[i] == ' :' :
            rule.append(5)
            word.append(s)
            s = ""
        elif keyword[i] == '.' :
            rule.append(4)
            word.append(s)
            s = ""
        elif keyword[i] == ';' :
            rule.append(3)
            word.append(s)
            s = ""
        elif keyword[i] == ':' :
            rule.append(2)
            word.append(s)
            s = ""
        elif keyword[i] == ',' :
            rule.append(1)
            word.append(s)
            s = ""
        else :
            s += keyword[i]
        i += 1
    # print(word)
    # print(rule)

    for i in range(0, sesi_mod) :
        rule = geserBalikMarker(rule)

    print("WORD")
    print(word)
    
    print("RULE")
    print(rule)

    asci_arr = []


    for i in range(0, len(rule)) :
        # print(asci_arr)
        if i%2 == 0 :
            if rule[i] == 1 :
                asci_arr.insert(0, word[i][-1])
                asci_arr.insert(0, word[i][0])
            elif rule[i] == 2 :
                asci_arr.insert(0, word[i][-1])
                asci_arr.insert(0, '*' + word[i][0])
            elif rule[i] == 3 :
                asci_arr.insert(0, '*' + word[i][-1])
                asci_arr.insert(0, word[i][0])
            elif rule[i] == 4 :
                asci_arr.insert(0, '*' + word[i][-1])
                asci_arr.insert(0, '*' + word[i][0])
            else :
                asci_arr.insert(0, word[i][0])
        else :
            if rule[i] == 1 :
                asci_arr.insert(0, word[i][0])
                asci_arr.insert(0, word[i][-1])
            elif rule[i] == 2 :
                asci_arr.insert(0, '*' + word[i][0])
                asci_arr.insert(0, word[i][-1])
            elif rule[i] == 3 :
                asci_arr.insert(0, word[i][0])
                asci_arr.insert(0, '*' + word[i][-1])
            elif rule[i] == 4 :
                asci_arr.insert(0, '*' + word[i][0])
                asci_arr.insert(0, '*' + word[i][-1])
            else:   
                asci_arr.insert(0, word[i][0])

    print("ASCI ARR")
    print(asci_arr)


    decoded = decodeAsciArr(asci_arr)

    print("DECODED ASCI")
    print(decoded)
    

    biner = convertAsciArrToBiner(decoded)

    print("BINER")
    print(biner)
    # print(biner)
    actual_biner = ""

    for b in biner :
        actual_biner += b[2:7]

    print("ACTUAL BINER")
    print(actual_biner)


    otp = []

    # print(kalimat)
    words = kalimat.split(' ')

    i = 0
    idx_word = 0
    last_idx = 0
    
    n = len(actual_biner)
    # print(n)
    # print("loop")
    while i <  n:
        if actual_biner[i] == '1' :
            current_idx = last_idx + 1
            last_idx = current_idx + 4
            if last_idx > n:
                last_idx = n
            # last_idx = current_idx + len(words[idx_word])
            biner = actual_biner[current_idx:last_idx]
            found = int(biner, 2) - 1
            # print(biner)
            # print(found)
            # print(words[idx_word])
            # print(current_idx)
            # print(last_idx)
            word = words[idx_word]
            otp.append(word[found])
            i += len(biner)
        else :
            last_idx += 1
        i += 1
        idx_word += 1


    # for i in range(0, len(kalimat)) :
    #     if actual_biner[i] == '1' :
    #         otp.append(kalimat[i])



    print("+++ MAPPING TABLE +++")
    print(mapping_digit)

    print("+++ OTP +++")                                                                                                                                   
    print(otp)            





    

