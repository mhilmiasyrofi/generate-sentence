from __future__ import print_function
import random
from utils import *

data = []
otp = []
generated_sentence_array = []
generated_sentence = ""

if __name__ == '__main__':
    readData(data)

    failedFile = open("failed.txt", "w")
    successFile = open("success.txt", "w")
    success = False
    for n in range(0, 999999):
    # for n in range(105000, 105511):
        otp = fixOtp(n)
        print(n)
        generated_sentence_array = []
        generated_sentence = ""
        generated_sentence_array = generateSentence(data, otp)
        if len(generated_sentence_array) == 6 :
            success = True

        if success:
            print("success\n")
            for i in range(0, len(generated_sentence_array)):
                generated_sentence += generated_sentence_array[i]
                if i != len(generated_sentence_array) - 1:
                    generated_sentence += " "
            successFile.write(str(n))
            successFile.write("\n")
            successFile.write(generated_sentence)
            successFile.write("\n")
            successFile.write("\n")
        else :
            print("failed\n")
            failedFile.write(str(n))
            failedFile.write("\n")
            failedFile.write("\n")

    successFile.close()
    failedFile.close()

