As the internet and digital technology become a bigger part of our lives, more of our data becomes publicly accessible, leading to questions about privacy. 
So, how do we interact with the growing digital world without compromising the security of our information and our right to privacy?
Imagine that you want to learn a new language. 
You search 'Is German a difficult language?' on your phone. You click on a link and read an article with advice for learning German.
There's a search function to find German courses, so you enter your city name. It asks you to activate location services to find courses near you.
You click 'accept'.
You then message a German friend to ask for her advice. When you look her up on social media, an advertisement for a book and an app called German for Beginners instantly pops up. 
Later the same day, while you're sending an email, you see an advert offering you a discount at a local language school. 
How did they know? The simple answer is online data. 
At all stages of your search, your devices, websites and applications were collecting data on your preferences and tracking your behaviour online. 
'They' have been following you.
In the past, it was easy for people to keep track of their personal information. 
Like their possessions, people's information existed mostly in physical form: on paper, kept in a folder, locked in a cupboard or an office. 
Today, our personal information can be collected and stored online, and it's accessible to more people than ever before. 
Many of us share our physical location, our travel plans, our political opinions, our shopping interests and our family photos online – as key services like ordering a takeaway meal, booking a plane, taking part in a poll or buying new clothes now take place online and require us to give out our data. 
Every search you make, service you use, message you send and item you buy is part of your 'digital footprint'. 
Companies and online platforms use this 'footprint' to track exactly what we are doing, from what links we click on to how much time we spend on a website.
Based on your online activity, they can guess what you are interested in and what things you might want to buy. 
Knowing so much about you gives online platforms and companies a lot of power and a lot of money. 
By selling your data or providing targeted content, companies can turn your online activity into profit. 
This is the foundation of the growing industry of digital marketing.
Some of the time our personal data is shared online with our consent. 
We post our birthday, our photographs and even our opinions online on social media. 
We know that this information is publicly accessible. 
However, our data often travels further than we realise, and can be used in ways that we did not intend. 
Certain news scandals about data breaches, where personal data has been lost, leaked or shared without consent, have recently made people much more aware of the potential dangers of sharing information online.
So, can we do anything to protect our data? Or should we just accept that in fact nothing is 'free' and sharing our data is the price we have to pay for using many online services? As people are increasingly aware of and worried about data protection, governments and organisations are taking a more active role in protecting privacy.
For example, the European Union passed the General Data Protection Law, which regulates how personal information is collected online. However, there is still much work to be done.
As internet users, we should all have a say in how our data is used. 
It is important that we pay more attention to how data is acquired, where it is stored and how it is used. 
As the ways in which we use the internet continue to grow and change, we will need to stay informed and keep demanding new laws and regulations, and better information about how to protect ourselves. 
Safer Internet Day is an ideal time to find out more about this topic. 
Thirty years ago, this website and any other websites were impossible to imagine.
Some people had computers in their homes, but they didn't use them for much. 
Maybe they just used them to play games or type letters to print out and send by post. But, in 1989, a British computer scientist called Tim Berners-Lee changed everything and one of the modern world's greatest inventions was born: the World Wide Web.
Now, let's make sure you're not confused about something. 
We're not talking about the beginning of the internet itself. Most people use the words internet and Web as if they're the same thing.
But, in fact, the internet is much older than the Web, and they're two different things. 
The internet was developed in the early 1970s by Vint Cerf and Bob Kahn. 
It is basically a huge network made up of smaller networks of computers that deliver packets of information to other computers. 
When this information is in the form of webpages, that's the World Wide Web.
So, in 1989, when Tim Berners-Lee was working at CERN (the European Organisation for Nuclear Research) in Switzerland, the internet already existed. 
But it was nothing like it is now because there were no webpages. 
Hard to imagine, isn't it? Email also already existed (Queen Elizabeth II famously sent an email in 1976) and so did the idea of domain names, for example britishcouncil.org. 
Another tool that already existed was hypertext to jump from one document to another. 
But, without the Web, none of it was as useful as it is now. 
Berners-Lee got very frustrated at CERN because all the scientists had different kinds of computers. 
You could connect the computers with cables, but they couldn't 'speak' to each other.
If you wanted information, you had to know exactly which computer that information was on and sit down in front of it and log in. 
Berners-Lee wrote a report that suggested a way of putting the internet, domain names and hypertext together into one system. 
This 'imaginary information system which everyone can read' was later called the World Wide Web (and that's why website addresses start with 'www'). 
At the time, his idea was so abstract that his boss called it 'vague but exciting'. 
Two years later, in 1991, the world's first website was built at CERN: http://info.cern.ch (the site you can see now is a copy made in 1992).
Today, thirty years later, that idea is still exciting. 
The Web is just part of the internet, but it is the part that connects us to the rest of the world. 
The telephone meant that one person could connect with one other person. 
Television meant that one person's ideas could reach millions of people in their homes. 
But, with the Web, everyone who has internet access is connected and anyone can contribute to the information on it.
The idea behind the Web is to connect people and help them understand each other.
But not everyone in the world has internet access – only 55 per cent of people, according to internetworldstats.com. 
Half the world's population lives in Asia, but only half of Asian people have the internet.
In North America, 95 per cent of people have internet access and so do 85 per cent of Europeans. 
North America and Europe make up only 15 per cent of the world's population, but together they make up 25 per cent of the world's internet users. 
Compare this with Africa, which makes up 17 per cent of the world's population, but only 36 per cent of its people have internet access. 
The digital divide isn't only geographical. 
If we compare men's and women's access to the internet, World Wide Web Foundation research shows that women are less likely to use the internet in many poor urban communities – 37 per cent of women versus 59 per cent of men surveyed were internet users. 
If you're reading this, you're part of the half of the world that has access to this powerful tool. 
Perhaps our job for the next thirty years is to make the Web available to the other half of the world and to help people use it to their best advantage.
The way we approach business is changing. 
As we continually pivot to keep pace with rapidly evolving technology, individual departments within an organization are becoming as agile as the larger companies themselves. 
IT departments are experiencing tremendous changes as their roles expand to impact customer service, sales, and even business strategies. 
As a result, organizations are increasingly turning IT into a driving force in all aspects of business.
One thing to understand when managing the growing pains of IT is that it’s no longer the work of a single department. 
Research from Accenture found that 34% of companies see the IT department as the main driver of innovation, which is down from 71% two years ago. 
I see the reason for this as a change in IT infrastructure itself. 
The modern IT worker is a tech-savvy innovator who creates change across the organization’s entirety, not just a single department. 
As employees create change across all levels of an organization, the new face of IT defies boundaries.
IT workers aren’t just facilitating business goals; they’re driving change at an organizational level. 
CEOs are leaning on IT to deliver a competitive advantage as much as they do for a marketing strategy. 
It seems to me that this shift is directly related to “The Internet of Everything” movement, which refers to the near future in which nearly every aspect of our lives will be connected to the Internet. 
Since our reliance on technology increases with each passing day, IT is steadily moving to the front end—not the back—of business.
The IT world is experiencing a need for upskilling in its employees, especially as the role of IT continues to evolve into a more interdisciplinary field. 
Machines are becoming increasingly intelligent, so we need to train our employees working alongside them to remain agile.
Just as workers across the enterprise must become more tech-savvy, workers in the IT department must understand the nuances of marketing.
CIOs must increasingly hire employees with business acumen, not just good technology support skills. Enterprises will need to adjust in response to a changing talent ecosystem.
A few months ago, I expressed every brand’s need for continuity among marketing, sales and customer service. 
It’s clear that IT has a finger or two in all these pies. For a company to embrace these connections, it must see IT not as the lone geek pack, but rather as a field that supports the ebb and flow of business.
IT departments are now the linchpin of agile organizations. 
Their influence is spreading within the organization, and CIOs must transform the workforce along with it. 
Consider how you’ll construct a workforce and culture that drives innovation, as well as one that will facilitate the customer experience and encourage business growth. 
It seems like a tall order, but CIOs are confronted with these issues as the world around us becomes smarter. 
As CEOs realize their role in successfully launching new products, I believe we’ll rely on IT with increasing frequency.
Sourcing so-called “jack-of-all-trade” talent for the new business ecosystem is one thing, but CIOs must also consider legacy systems as they relate to company goals. 
Reskilling existing teams will be necessary to maintain agility, so it’s important to construct a team of employees who can handle the peaks and valleys of business.
This all sounds easier said than done, but there’s one prevailing attitude in business that will never change: adapt or fail.
Current employees must be on board with the changing face of IT in business, while new talent should enter a role with clear expectations. 
It’s the CIO’s role to facilitate these processes by reskilling legacy processes and seeking out adaptable talent. 
Collaboration with the CMO and CEO will also be key in identifying and addressing skill gaps.
Technology is constantly evolving, and we’re at a juncture in which IT drives operations at every level of an organization. 
Changing the old model of IT as a separate entity into a breathing part of the business will mean a thriving enterprise—and one that runs smoothly.
Benefiting from globalisation and tectonic shifts in science and technology, African countries have been able to “leapfrog” some of the stages of development that other nations had to plod their way through.
The transition from landlines to mobile telephony is a case in point.
In the case of information and communications technology (ICT), most African countries are centred in two areas: constructing ICT-related infrastructure and integrating its application into the broader development agenda. 
The intersection of these areas suggests an increasingly prominent move towards developing smart cities.
In light of this, the two principles that should be guiding investments into ICT are safety and sustainability, where three major types of ICT are likely to play an instrumental role: surveillance cameras, light emitting diodes (LEDs) and geographic information systems (GIS). 
Applications of surveillance cameras are wide-ranging, especially with regard to public safety, environmental monitoring and traffic control.
For example, when Chinese telecoms firm Huawei and Safaricom recently collaborated on Kenya’s “Safe City” solution to deal with terrorism in Nairobi and Mombasa, 1,800 surveillance cameras were installed. As the demand for security-related monitoring systems continues to rise in a tumultuous geopolitical landscape, technical updates of surveillance cameras could be a huge potential market for investments between China and Africa.
LEDs are essential for streetlights, an integral component of transport infrastructure. 
LEDs can be controlled remotely to tune lighting in accordance with vehicle/pedestrian movement and weather conditions or in response to unexpected events.
Market intelligence firm IDC predicts 180m global LED streetlight conversions worldwide by 2019 and spending of $80bn.
There is an opportunity for collaboration between Chinese companies and investors and African-based partners to harness technology advances in China. 
Investors must grab this opportunity and put money into related products, such as semiconductor chips.
While smart cities are built on the backbone of solid infrastructure and tight security, they will be impossible to sustain if they are unable to feed themselves. 
Here, GIS has a crucial role to play in developing “smart food” in both rural and urban spaces, offering capabilities that extend to drought warning systems and food security supervision.
Home to 60% of the world’s arable land, Africa has the potential to become the future global food supplier. 
In this case, GIS will inevitably be a core technology at play.
Over the next two decades, Africa has a great opportunity to disrupt traditional paths to growth and prosperity through innovative approaches to science and technology.
The success of this disruption is dependent on the continent’s continued successful relationship with China. 
Advances in technology made by China present an opportunity for deployment and investment partnerships with African countries in a real way.
Isaac Fokuo is founder and chairman of the Sino-Africa Centre of Excellence and a 2014 Desmond Tutu Leadership Fellow.
