from __future__ import print_function
import random
import readkbbi
from utils import readData
import copy

data = []

if __name__ == '__main__':
    readData(data)
    triWordFile = open("tri-word.txt", "w")
    for d in data :
        print(d.word)
        for i in range(0, len(d.prev)) :
            kalimat = d.prev[i] + " " + d.word + " " + d.next[i] + "\n"
            triWordFile.write(kalimat)

    triWordFile.close()