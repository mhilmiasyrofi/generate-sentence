import string

def getDataFromFile(filename):
	fInput = open(fileName)
	data = fInput.read()
	fInput.close()
	return data

def countCharOccurences(data, maxAlphabet):
	alphabetCounter = []

	for character in string.ascii_lowercase:
		totalCharOccurences = data.count(character) + data.count(character.upper())
		alphabetCounter.append(totalCharOccurences)

	sortedList = sorted(alphabetCounter, reverse=True)[0:maxAlphabet]

	print (str(maxAlphabet) + " buah huruf yang paling sering muncul")
	for i in sortedList:
		idx = alphabetCounter.index(i)
		print (chr(idx + 65) + ' ' + str(i))
	return

#
# MAIN PROGRAM
#
MAX_ALPHABET_DISPLAYED = 27
fileName = input("Silakan masukkan nama file corpus: ")
fileContent = getDataFromFile(fileName)
countCharOccurences(fileContent, MAX_ALPHABET_DISPLAYED)
