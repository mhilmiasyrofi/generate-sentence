class Word:
    word = ""
    eja = []

    def __init__ (self, word = "blank", eja = []) :
        self.word = word
        self.eja = eja

    def printWord(self) :
        print(self.word)
        print(self.eja)

def readKBBI() :
    words = []
    # with open("KBBI.txt", "r") as file :
    #     for line in file:
    #         # print(line)
    #         # print(len(line))
    #         if not line == "" :
    #             arr = line.split(' ')
    #             # print(arr)
    #             # print(arr[0])
    #             # print(arr[1].split('-'))
    #             word = Word(arr[0], arr[1].split('-'))
    #             words.append(word)
    with open("Oxford_English_Dictionary.txt", "r") as file:
        for line in file:
            # print(line)
            # print(len(line))
            if not line == "" :
                arr = line.split(' ')
                # print(arr)
                # print(arr[0])
                # print(arr[1].split('-'))
                if len(arr[0]) >= 4 and not '\'' in arr[0] and not '.' in arr[0] and not '-' in arr[0]:
                    word = Word(arr[0].lower(), [])
                    words.append(word)
    return words

# if __name__ == '__main__':
#     words = readKBBI()
#     for w in words:
#         w.printWord()


