from __future__ import print_function
import random
import readkbbi
from readkbbi import readKBBI
from utils import *
import copy
import datetime


data = []
otp = []
generated_sentence_array = []
generated_sentence = ""

# corpusFileName = "last-corpus.txt"
corpusFileName = "english-corpus.txt"

if __name__ == '__main__':  
    data = readData(corpusFileName)
    success_sentence = False
    success_keyword = False
    n_otp = 0
    mtable = []
    
    sentence = []
    sesi = 0
    n_sesi = 100

    start_time = datetime.datetime.now()
    finish_time = datetime.datetime.now()
    while sesi < n_sesi :
        sesi_mod = sesi**n_sesi % 7 % 6
        # print(sesi)
        # print(sesi_mod)
        binary = None
        asci = None
        while not success_sentence or not success_keyword :
            start_time = datetime.datetime.now()
            kalimat = ""    
            mtable = generateMappingTable()
            otp, n_otp = generateOtp(mtable)
            # otp = ['a', 'i', 'e', 'n', 'r', 's']
            
            # print("mapping otp: ", end = '')
            # print(otp)
            sentence = []
            i = 0
            sentence = generateSentence(data, otp, mtable)
            success_sentence = False
            success_keyword = False
            if len(sentence) != 0 :
                success_sentence = True
                # print()
                # print("++KALIMAT++")
                for s in sentence :
                    kalimat += s['word'] + ' '
                    # print(s['word'], end= ' ')
                # print()

                binary = ""
                # print(sentence)
                for s in sentence :
                    # for i in range(0, len(s['word'])) :
                    #     if i == s['idx'] :
                    #         binary += '1'
                    #     else :
                    #         binary += '0'
                    # binary += '0'
                    if s['idx'] == -1 :
                        binary += '0'
                    else :
                        binary += '1'
                        biner = encodeBiner(s['idx'], s['word'])
                        binary += biner
                # print()    
                # print("++BINARY++")
                # print(binary)

                asci_arr = decodeBinarytoAscii(binary)
                # print(asci_arr)
                
                # print()    
                # print("++ASCI++")
                # print(asci_arr)
                asci = encodeAsciArr(asci_arr)
                # asci = "p*ba*b*bhd*bp*bd"
                asci = asci.lower()
                # print(asci)
                
                words = readKBBI()
                # for w in words:
                #     w.printWord()

                random.shuffle(words)

                # print()
                # print("++KEYWORD++")
                keyword = generateKeyword(asci, words, sesi_mod)
                
                kw = ""

                # print("KEYWORD")
                # print(len(keyword))
                # print(asci[-2])

                if len(keyword) != 0 and asci[-2] != '*':
                    success_keyword = True
                    # print(len(keyword))
                    # print(keyword)

                    # for k in keyword :
                        # kw += k.word + k.rule
                    #     print(k.word, end='')
                    #     print(k.rule, end='')
                    # print()
                    
                    for y in range(0, sesi_mod) :
                        keyword = geserKeyword(keyword, sesi_mod)


                    for k in keyword :
                        kw += k.word + k.rule
                    #     print(k.word, end='')
                    #     print(k.rule, end='')
                    # print()
                    # print()

                    # print()
                    # print("++GABUNGAN++")
                    gabungan = kalimat  + "#" + kw
                    length = len(gabungan)
                    # print("Panjang= ", end='')
                    # print(length)
                    if length  > 140 :
                        success_keyword = False
                        # print("Generate keyword gagal, gabungan melebihi 140 karakter")
                        # print("Generate ulang")

        print("Sesi ", end='')
        print(sesi)
        print("+++ Encode +++")                                                                                                                                   
        print(n_otp)
        print(otp)
        print(binary)
        print(asci)
        print(gabungan)
        finish_time = datetime.datetime.now()
        difference  = finish_time - start_time
        print("+++ Waktu Encode+++")                                                                                                                                   
        print(difference)
         
        print("+++ Decode +++")                                                                                                                                   
        start_time_decode = datetime.datetime.now()
        decode(gabungan, sesi_mod, mtable)
        finish_time_decode = datetime.datetime.now()

        sesi += 1

        print("+++ Waktu Decode+++")                                                                                                                                   
        print(finish_time_decode-start_time_decode)

        success_sentence = False
        success_keyword = False
        print()
        print()



